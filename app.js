const btnOne = document.getElementById('btnOne');
const btnTwo = document.getElementById('btnTwo');
const btnThree = document.getElementById('btnThree');

const container = document.getElementById('container');

btnOne.addEventListener('click', function() {
	const div = document.createElement('div');
	div.className = 'divOne';
	if (container.children.length > 0) {
		container.removeChild(container.firstChild);
	}
	container.append(div);
});

btnTwo.addEventListener('click', function() {
	const div = document.createElement('div');
	div.className = 'divTwo';
	if (container.children.length > 0) {
		container.removeChild(container.firstChild);
	}
	container.append(div);
});

btnThree.addEventListener('click', function() {
	const div = document.createElement('div');
	div.className = 'divThree';
	if (container.children.length > 0) {
		container.removeChild(container.firstChild);
	}
	container.append(div);
});
